<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\category;
use App\comment;
class PublicPostController extends Controller
{
    public function show(){
        $data = Post::paginate(6);
        $cats = category::all();
        return view('pages.home', compact('data', 'cats'));
    }
    public function viewPost(Post $data){
        $user = User::all();
        return view('pages.viewPost', compact('data', 'user'));
    }
    public function computers(){
        $data = Post::where('cat_id', 1)->get();
        return view('pages.computers', compact('data'));
    }
        public function phones(){
        $data = Post::where('cat_id', 2)->get();
        return view('pages.phones', compact('data'));
    }
            public function modules(){
        $data = Post::where('cat_id', 3)->get();
        return view('pages.phones', compact('data'));
    }


}
