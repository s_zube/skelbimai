<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\category;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
class PostController extends Controller
{
 public function __construct()
 {
    $this->middleware('auth');
 }
    public function store(Request $request){
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required'
        ]);
        /*Post::create(request()->all());*/
            $title = $request->input('title');
            $body = $request->input('body');
            $cat_id = $request->input('cat_id');
            $userid = $request->input('user_id');
            /*Storage::disk('public')->put($piclink, File::get($img));*/
        Post::create([
            'user_id'=>auth()->id(),'title'=>$title, 'body'=>$body, 'cat_id'=>$cat_id
        ]);

        return redirect('/');
    }

    public function newPost(){
		$data = Post::all();
        $cats = category::all();
        return view('pages.new-entry', compact('data', 'cats'));
    }

    public function editPost(Post $data){
        $cats = category::all();
        if(Gate::denies('edit-post', $data)){
            return view(page.restriction);
        }
        return view('pages.editPost', compact('data', 'cats'));
    }

    public function upEditPost(Request $request, Post $data){
        Post::where('id', $data->id)->update($request->only(['title','cat_id', 'body']));
        return redirect('/');
    }
    public function deletePost(Post $data){
        $data->delete();
        return redirect('/');

    }
    public function dashboard(){
        $data = DB::table('posts')->where('user_id' , Auth::id())->get();
        return view('pages.dashboard', compact('data'));
    }

        public function deleteCatPost(Post $data){
        $data->delete();
        return redirect('/computers');
    }
}