<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\comment;
use App\Post;
class CommentController extends Controller
{
    public function upComment(Post $data)
    {
        comment::create([
            'user_id' => auth()->id(),
            'body' => request('body'),
            'post_id' => $data->id
        ]);
    return back();
    }

}
