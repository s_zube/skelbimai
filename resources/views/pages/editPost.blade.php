@extends ('layouts/main')
@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="form-area">
                <form role="form" method="post" action="/postupdate/{{$data->id}}/">
                    {{csrf_field()}}
                    {{METHOD_FIELD('PATCH')}}
                    <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">Redaguoti Skelbimą</h3>
                    <div class="form-group">
                        <input type="text" class="form-control" name="title" for="title" id="title" value="{{$data ->title}}" required>
                    </div>
                                    <select class="form-control form-control-sm" for="cat_id" id="cat_id" name="cat_id">
                @foreach($cats as $cat)
  <option  value="{{($cat->id)}}">         
                               <h3>{{($cat->name)}}</h3>

            @endforeach</option>
</select>
                    <div class="form-group">
                        <textarea class="form-control" type="textarea" name="body" id="body" rows="7">{{$data->body}}</textarea>
                        <span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
                    </div>

                    <input type="submit" name="submit" value="Siųsti"></input>
                </form>
            </div>
        </div>
    </div>
@endsection