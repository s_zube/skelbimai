@extends ('layouts/main')
@section('content')
         <div class="thumbnail">
     <div class="caption">
   <h2>{{$data->title}}</h2>
   <p>Autrius: <i>{{\App\User::find($data->user_id)->name}}</i></p>
   <p>Kategorija: <i><b>{{\App\category::find($data->cat_id)->name}}</b></i></p>
   <p>{{$data->body}}</p>
     </div>
    @if(Auth::id()==$data->user_id)
        <a class="btn btn-default" href="/editpost/{{$data->id}}/edit" role="button">Edit</a>
        <a class="btn btn-default" href="/deletepost/{{$data->id}}/delete" role="button">Delete</a>
    @endif
 </div>

   <div class="comments container">
      <h2>Komentarai</h2>
      <ul class="list-group">
         @foreach($data->comments as $comment)

<div class="col-sm-5 col-md-12">
<div class="panel panel-default">
<div class="panel-heading">
<strong>{{$comment->name}}</strong> <span class="text-muted">commented {{$comment->created_at}}</span>
</div>
<div class="panel-body">
{{$comment->body}}
</div><!-- /panel-body -->
</div><!-- /panel panel-default -->
</div><!-- /col-sm-5 -->

            @endforeach

      </ul>
   </div>




   <hr>
   <div class="card">
      <div class="card-block">
         <form method="POST" action="/post/{{$data->id}}/comment">
            {{csrf_field()}}
            <div class="form-group">
               <textarea name="body" placeholder="Jūsų komentaras" class="form-control">

                  </textarea>
            </div>
            <div class="form-group">
               <button type="submit" class="btn btn-primary">Siųsti</button>
            </div>
         </form>
      </div>
   </div>


@endsection