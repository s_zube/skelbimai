@extends ('layouts/main')
@section('content')
<div class="container new-post ">
    <div class="">
        <div class="form-area">

            <form role="form" method="post" action="/savepost">
                {{csrf_field()}}
                <br style="clear:both">
                <h3 style="margin-bottom: 25px; text-align: center;">Naujas skelbimas</h3>

                <div class="form-group">

                    <input type="text" class="form-control" name="title" for="title" id="title" placeholder="title" required>

                </div>
				<select class="form-control form-control-sm" for="cat_id" id="cat_id" name="cat_id">
				@foreach($cats as $cat)
  <option  value="{{($cat->id)}}">         
							   <h3>{{($cat->name)}}</h3>

            @endforeach</option>
</select>
<br>
                <div class="form-group">

                    <textarea class="form-control" type="textarea" name="body" id="body" placeholder="Message" rows="7"></textarea>
                    
                </div>
        
        <!--file input example -->
        

                <input type="submit" name="submit" value="Siųsti"></input>
            </form>
        </div>

    </div>
</div>

@endsection