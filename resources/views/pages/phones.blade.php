@extends ('layouts/main')
@section('content')


   <div class="container">
   <div class="thing">
   <div class="row">
@foreach ($data as $info)
  <div class="col-sm-6 col-md-6">
    <div class="thumbnail">
      <div class="caption">
        <h2>{{$info->title}}</h2>
            <p>Autorius <i>{{\App\User::find($info->user_id)->name}}</i></p>
      <p>Kategorija: <i>{{\App\category::find($info->cat_id)->name}}</i></p>
        <p>{{str_limit($info->body), 100}}</p>
        <p><a class="btn btn-default" href="/viewpost/{{$info->id}}" role="button">Daugiau &raquo;</a></p>
      </div>
    </div>
  </div>
  @endforeach
</div>
</div>
</div>



@endsection