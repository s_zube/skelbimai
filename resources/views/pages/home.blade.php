@extends ('layouts/main')

@section('content')
	
<form>
  <div class="form-row container">
 
    <div class="form-group">
      <label for="inputState">Kategorija</label>
      <select id="inputState" class="form-control" onchange="location = this.value;">
        <option selected>Pasirinkite</option>
        
        @foreach ($cats as $cat)
		<option value="{{$cat->name}}">{{$cat->name}}</option>
        @endforeach
      </select>
    </div>
</div>
</form>


<div class="container">
	<div class="thing">
	<div class="row">
@foreach ($data as $info)
  <div class="col-sm-6 col-md-6">
    <div class="thumbnail">
      <div class="caption">
        <h2>{{$info->title}}</h2>
        	   <p>Autorius <i>{{\App\User::find($info->user_id)->name}}</i></p>
	   <p>Kategorija: <i>{{\App\category::find($info->cat_id)->name}}</i></p>
        <p>{{str_limit($info->body), 100}}</p>
        <p><a class="btn btn-default" href="/viewpost/{{$info->id}}" role="button">Daugiau &raquo;</a></p>
      </div>
    </div>
  </div>
  @endforeach
</div>
</div>
{{ $data->links() }}
</div>



@endsection