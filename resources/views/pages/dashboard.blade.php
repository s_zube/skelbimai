@extends ('layouts/main')
@section('content')
  <div class="container">
  <h2>Dashboard</h2>
  </div>
    @foreach ($data as $info)
  <div class="col-sm-6 col-md-6">
    <div class="thumbnail">
      <div class="caption">
        <h2>{{$info->title}}</h2>
            <p>Autorius <i>{{\App\User::find($info->user_id)->name}}</i></p>
      <p>Kategorija: <i>{{\App\category::find($info->cat_id)->name}}</i></p>
        <p>{{str_limit($info->body), 100}}</p>
        <p><a class="btn btn-default" href="/viewpost/{{$info->id}}" role="button">Daugiau &raquo;</a></p>
    @if(Auth::id()==$info->user_id)
        <a class="btn btn-default" href="/editpost/{{$info->id}}/edit" role="button">Edit</a>
        <a class="btn btn-default" href="/deletepost/{{$info->id}}/delete" role="button">Delete</a>
    @endif
      </div>
    </div>
  </div>

    @endforeach




@endsection

