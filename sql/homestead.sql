-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018 m. Bal 15 d. 17:59
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.2.3-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homestead`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Kompiuteriai', NULL, NULL),
(2, 'Telefonai', NULL, NULL),
(3, 'Priedai', NULL, NULL);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `body`, `post_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'dddddddddddddddddddddd', 1, '2018-03-27 15:49:41', '2018-03-27 15:49:41'),
(2, 1, 'Siun2iasm', 1, '2018-04-03 18:46:46', '2018-04-03 18:46:46'),
(3, 1, 'LAbas', 2, '2018-04-04 14:56:45', '2018-04-04 14:56:45'),
(4, 1, 'Dar badnyams', 2, '2018-04-04 15:57:20', '2018-04-04 15:57:20'),
(5, 1, 'KOmewntajgegh', 6, '2018-04-07 09:44:47', '2018-04-07 09:44:47'),
(6, 1, 'Bandymas', 2, '2018-04-09 17:46:43', '2018-04-09 17:46:43'),
(7, 3, ':LAbas', 2, '2018-04-09 17:48:36', '2018-04-09 17:48:36'),
(8, 1, 'BAndyamsasfdgfe', 2, '2018-04-09 18:05:50', '2018-04-09 18:05:50'),
(9, 1, 'dar', 2, '2018-04-15 10:00:09', '2018-04-15 10:00:09'),
(10, 3, 'ffe', 5, '2018-04-15 10:42:36', '2018-04-15 10:42:36'),
(11, 3, 'cw', 4, '2018-04-15 10:43:04', '2018-04-15 10:43:04'),
(12, 3, 'dw', 5, '2018-04-15 10:43:31', '2018-04-15 10:43:31'),
(13, 3, 'dw', 3, '2018-04-15 10:45:47', '2018-04-15 10:45:47'),
(14, 3, 'dw', 4, '2018-04-15 11:29:53', '2018-04-15 11:29:53'),
(15, 1, 'Noreciau is jusu nusipirkti', 8, '2018-04-15 16:22:41', '2018-04-15 16:22:41'),
(16, 1, 'dw', 2, '2018-04-15 16:34:58', '2018-04-15 16:34:58');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2018_03_12_091049_create_tasks_table', 2),
(18, '2014_10_12_000000_create_users_table', 3),
(19, '2014_10_12_100000_create_password_resets_table', 3),
(20, '2018_03_19_130529_create_posts_table', 3),
(21, '2018_03_27_134127_create_comments_table', 3),
(22, '2018_04_04_135036_create_categories_table', 4);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `body`, `cat_id`, `created_at`, `updated_at`) VALUES
(2, 1, 'Dar vienas bandymas', 'Dar vienas bandymasDar vienas bandymasDar vienas bandymasDar vienas bandymasDar vienas bandymasDar vienas bandymasDar vienas bandymasDar vie', 1, '2018-04-03 18:47:41', '2018-04-03 18:47:41'),
(3, 1, 'Bandymas 2', 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 2, '2018-04-04 13:59:12', '2018-04-04 13:59:12'),
(4, 3, 'BAndymas 3', 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 2, '2018-04-04 14:12:48', '2018-04-04 14:12:48'),
(5, 3, 'Nario skalbimas', 'Superkame senus automobilius!\r\nAutomobiliai gali būti įvairios techninės būklės. Daužti,nevažiuojantys,be tech. apžiūros ar surūdija siūlyti įvairius variantus.\r\nPasiimame patys,padedame sutvarkyti reikiamus dokumentus,atsiskaitome iš karto.\r\nSusisiekite su mumis ir mes atsakysime į visus jums rūpimus klausimus.', 1, '2018-04-15 10:42:23', '2018-04-15 10:42:23'),
(8, 1, 'Nauji \"Riedžiai\" Mirtų g. 55, Aleksotas', 'Metų garantija nuo gamyklinių gedimų. Patys atliekame remontą.\r\n\r\nRiedžius - pristatome visoje Lietuvoje ir užsienio šalims. Kurjeriu. Autobusu. Paštu. Paštomatu.\r\n\r\n8 673 49 129 \r\nPasiteiravimui ar patarimui.\r\n\r\n\r\nRiedžio kartingas: 35€. \r\n\r\n7 colių - pilnas komplektas (bluetooth, led, pultelis) - 115€. \r\n7 colių - pilnas komplektas plius robotas, riedis stovi horizontaliai - 125€.\r\n7 colių (bluetooth, led) - 110€.\r\n8 colių - pilnas komplektas - 130€ \r\n9 colių robotas - pilnas komplektas - žvėriškai platūs ratai - kalnų riedis su stopais - tik 180 eurų. 5 spalvos (2-3 foto).\r\n9 colių led, bluetooth be roboto - 160 eurų. Privalumas minkštos padangos, kurių nereiks pūsti. \r\n10 colių - pilnas komplektas su pripučiamais ratais - 150€. \r\n10 colių - pilnas komplektas plius robotas, kuris įjungus riedį laiko jį horizontaliai - vaikučiui paprasta išmokti važiuot, didelis privalumas - riedžio neapdaužysite! Paskubėkit, kiekis ribotas! - 175€. Karts nuo karto yra!\r\n10 colių plačiais ratais Robotas (4 foto) - 185€. Yra daug spalvų.\r\n10 colių plačiais ratais speciali programėlė riedį valdyti - 200€. Daug spalvų.\r\nSegway 10 colių A8 su pripučiamais ratais- pilnas komplektas - 230€. Juodas arba baltas. \r\n\r\nRiedis su 10 colių PLAČIAIS ratais + BLUETOOTH + Centrinis valdymas + programėlė riedžiui valdyti - ROBOTAS 190 - 225€ (12 mėnesių garantija) (4 foto). Yra didelis spalvų pasirinkimas. \r\n\r\nROBOTAS- Riedis su kuriuo mokintis važiuoti nereikia. Riedis, kuris įjungus atsistoja ir stovi, pastūmus tiesiog rieda nekrenta. Riedis kurį galima įjungti arba išjungti telefonu. Galima reguliuoti daugybe nustatymų tokių kaip jautrumas, greitis ir t.t. Yra keletas rėžimų tarp jų PRO ir pradedantiesiems.\r\n\r\nPatogi rankena kuri padės kiekvienam vaikučiui be traumų išmokti riedį valdyti tik 15 €.\r\n\r\n*************************************************\r\n\r\nŠiuo metu gauta nauja siunta. Sandėlyje yra įvairių spalvų riedžių pasirinkimas!\r\n\r\n*******************************\r\n\r\nElektrinis Riedis Ratai 7colių, su BLUETOOTH, led ant sparnų ir pulteliu.\r\nKAINA 115€ \r\nPlatus spalvų pasirinkimas. \r\nŠis riedis turi bluetooth ryšį bei įmontuotus garsiakalbius, juo galėsite prisijungti prie savo telefono ir klausytis mėgstamos muzikos! Taip pat važiuojant riedis švies ryškiom led šviesom.\r\n\r\nBATERIJA: **SAMSUNG** \r\nAkumuliatoriaus talpa 4400mAh \r\nGalingumas: 700W(2*350) \r\nRatų skersmuo: 17cm \r\nSvoris: 10kg \r\nMinimali apkrova: 20kg \r\nMaksimali apkrova: 100kg \r\nGreitis: priklauso nuo apkrovos svorio, kelio salygų. Iki 15km/h \r\nĮkrovimo laikas: 1-2 valandos \r\nMaksimali įkalnė: 20 laipsnių.\r\n\r\n********************************\r\n\r\nElektrinis Riedis Ratai 8colių, BLUETOOTH, distancinis bei LED ant sparnų.\r\nKAINA 130€ \r\nPlatus spalvų pasirinkimas. \r\nŠis riedis turi bluetooth ryšį bei įmontuotus garsiakalbius, juo galėsite prisijungti prie savo telefono ir klausytis mėgstamos muzikos taip pat LED apšvietimas suteiks Jums žavesio. \r\n\r\nBATERIJA: **SAMSUNG** \r\nAkumuliatoriaus talpa 4400mAh \r\nGalingumas: 750W(2*375) \r\nRatų skersmuo: 20cm (8) \r\nSvoris: 12kg \r\nMinimali apkrova: 20kg \r\nMaksimali apkrova: 100kg \r\nGreitis: priklauso nuo apkrovos svorio, kelio salygų. Iki 15km/h.\r\nĮkrovimo laikas: 1-2 valandos \r\nMaksimali įkalnė: 20 laipsnių.\r\n\r\n********************************\r\n\r\nElektrinis Riedis 10colių pripučiami ratai, BLUETOOTH, distancinis. \r\nKAINA 150€ \r\nPlatus spalvu pasirinkimas.\r\nBATERIJA: **SAMSUNG** \r\nAkumuliatoriaus talpa 4400mAh \r\nGalingumas: 70pW(2*350) \r\nRatų skersmuo: 25cm (10) pripučiami\r\nSvoris: 13kg \r\nMinimali apkrova: 20kg \r\nMaksimali apkrova: 100kg \r\nGreitis: priklauso nuo apkrovos svorio, kelio salygų. Iki 18km/h.\r\nĮkrovimo laikas: 1-2 valandos \r\nMaksimali įkalnė: 20 laipsnių.\r\n\r\n**********************************************\r\n\r\nSegway A8 + distancinis pultelis bei ledai ant sparnų.\r\nKaina: 235€\r\n\r\nŠis segvėjus valdomas su rankena, yra LCD ekranas, kuris rodo baterijos likutį bei greitį. Be pultelio šis segway neveiks. Privalumas ir konfortiškumas važiuojant. \r\n\r\nGalingumas: 800W(2*400) \r\nRatų skersmuo: 25cm (10) \r\nBaterija: SAMSUNG ličio jonų \r\nSvoris: 13.5 kg \r\nMinimali apkrova: 25kg \r\nMaksimali apkrova: 120kg \r\nGreitis: priklauso nuo apkrovos svorio, kelio salygų. Iki 15km/h \r\nNuvažiuojamas atstumas (pilnai įkrovus): 5 iki 15 km. Priklauso nuo apkrovos, kelio salygų ir t.t.\r\nĮkrovimo laikas: 2-3 valandos \r\n\r\n***********************************************\r\n\r\nSamsung akumuliatorius: 30 eurai.\r\nPultelis: 1.5 eurai.\r\nPakrovėjas: 12 eurų.\r\n\r\n\r\nJei neapsisprendžiate ar nežinote kokį geriausiai pirkti, visada galime patarti ir padėti išsirinkti. Taip pat riedžiam suteikiam metų garantiją nuo gamyklinių gedimų ir duodame lietuvišką naudojimosi instrukciją.', 3, '2018-04-15 16:22:10', '2018-04-15 16:22:10'),
(9, 1, 'Kasdien Vežame Į/įš Vokietija,olandija,belgija.', 'Reguliariai vežame į Vokietiją įš visos Lietuvos keleivius siuntas\r\nKomfortiškos kelionės į/iš Vokietija \r\nSaugi ir rami kelionė ,mikroautobusai erdvūs.\r\nAudio,Video lankstomos sedynės .\r\nKiekvienam Pledas Kelionės metu.\r\nSkambinkite Tel\r\nLietuvoje.\r\n+37063810849 \r\n+37063810857\r\nVokietijoje.\r\n+4915145091262\r\n\r\nAntradieniais vežame iš visos Lietuvos į visą Vokietiją.\r\n\r\nKetvirtadieniais vežu iš visos Lietuvos į visą Vokietiją.\r\n\r\nŠeštadieniais vežam iš visos Lietuvos į visą Vokietija.\r\n\r\nPirmadieniais važiuoja iš visos Vokietijos į visą Lietuvą.\r\n\r\nKetvirtadieniais veža iš visos Vokietijos į visą Lietuvą.\r\n\r\nŠeštadieniais važame iš visos Vokietijos į visą Lietuvą.\r\n\r\nVežame iš visų Lietuvos miestų Keleivius ir siuntinius i visus Vokietijos miestus ,Vilnius Kaunas Klaipeda Šiauliai Panevėžys Alytus Marijampolė Telšiai Plungė Kėdainiai Jonava Ukmergė Tauragė Raseiniai Elektrėnai Kelmė Radviliškis Garliava Kretinga Šilutė Gargždai Kuršėnai Jurbarkas Vilkaviškis Lentvaris Grigiškės Prienai Joniškis Varėna Kaišiadorys Pasvalys Kupiškis Zarasai Skuodas Kazlų Rūda Širvintos Molėtai ir iš kitų Lietuvos miestų ir miestelių. Nuolatos vežame keleivius siuntinius į ir iš Vokietija.\r\n\r\nVežame į visus Vokietijos miestus , Važiuojame iš Visų Vokietijos miestų i visus Lietuvos miestus.Bayreuth, Bamberg, Brandenburg an der Havel, Bocholt, Celle, Aschaffenburg, Dinslaken, Aalen, Lippstadt, Landshut, Herford, Kempten (Allgu), Fulda, Plauen, Neuwied, Kerpen, Neubrandenburg, Weimar, Dormagen, Grevenbroich, Sindelfingen, Russelsheim, Herten, Rosenheim, Wesel, Garbsen, Bergheim, Unna, Schwbisch Gmund, Frankfurt (Oder), Friedrichshafen, Offenburg, Langenfeld (Rheinland), Greifswald, Hameln, Stolberg (Rheinland), Goppingen, Euskirchen, Eschweiler, Neu-Ulm, Hilden, Meerbusch, Hattingen, Sankt Augustin, Gorlitz, Pulheim, Baden-Baden, Waiblingen, Monchengladbach, Braunschweig, Chemnitz, Aachen, Halle (Saale), Magdeburg, Krefeld, Freiburg (Freiburg im Breisgau), Lubeck, Oberhausen, Erfurt, Mainz, Rostock, Kassel, Hagen, Saarbrucken, Hamm, Mullheim an der Ruhr, Ludwigshafen (Ludwigshafen am Rhein), Leverkusen, Osnabruck, Sollingen, Herne, Neuss, Heidelberg, Darmstadt, Paderborn, Regensburg, Ingolstadt, Wurzburg, Wolfsburg, Furth, Ulm, Offenbach am Main, Heilbronn, Pforzheim, Gottingen, Bottrop, Recklinghausen, Reutlingen, Koblenz, Remscheid, Jena, Trier, Erlangen, Moers, Cottbus (Chśebuz), Siegen, Hildesheim, Salzgitter, Kaiserslautern, Witten, Gtersloh, Gera, Iserlohn, Schwerin, Zwickau, Hanau, Ludwigsburg, Esslingen am Neckar, Ratingen, Marl, Konstanz, Villingen-Schwenningen, Velbert, Worms, Minden, Wilhelmshaven, Dorsten, Delmenhorst, Gladbeck, Castrop-Rauxel, Arnsberg, Rheine, Detmold, Marburg, Troisdorf, Ldenscheid, Lneburg, Bayreuth, Bamberg, Bocholt, Celle, Aschaffenburg, Dinslaken, Aalen, Lippstadt, Landshut, Herford, Fulda, Plauen, Neuwied,\r\n\r\n\r\nSkambinkite Tel\r\nLietuvoje.\r\n+37063810849 \r\n+37063810857\r\nVokietijoje.\r\n+4915145091262\r\nInfo@vezame-i-vokietija.lt', 2, '2018-04-15 16:24:27', '2018-04-15 16:34:22'),
(10, 1, 'Parduodu Xbox 360 nulaužtas, daug žaidimų, 2pultai', 'Konsolėje perrašyta operacinė sistema, todėl galima tiesiai į ją įsirašyti atsisiųstus žaidimus. \r\nNuotraukose matosi įrašyti žaidimai. \r\n\r\nKomplektacija: \r\nXbox 360 konsolė \r\nMatinimo blokas \r\n2 originalūs pulteliai \r\nHDMI laidas \r\nAV jungtis \r\nDėžutė\r\nKaina: 130 €', 1, '2018-04-15 16:26:53', '2018-04-15 16:26:53');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `address`, `email`, `phone`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Darbas1', 'Adresas1', 'Email1', 'Phone1', NULL, NULL, NULL),
(6, 'Darbas2', 'Adresas2', 'Email1@gmail.com', 'Phone2', NULL, NULL, NULL),
(7, 'Darbas3', 'Adresas3', 'Email3@gmail.com', 'Phone4', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sarunas', 'test@test.lt', '$2y$10$aGGYhb78gGumQ.Ca.Wg8cunpTpixVZFBWFO2Kg4xO0sGfwUVa/Z5a', 'Hr7PbxGbrxyd77yvVSwPKqe9khwvJFqDGt4JS2lKvTb4vytglgJUcHnFAS7d', '2018-03-27 15:23:20', '2018-03-27 15:23:20'),
(2, 'Test2', 'test2@test.lt', '$2y$10$rnAgh6X4PDltWxvRjEr/luR7hLp5jjo4qOjwcDqgx9Thbmf3CUP3m', NULL, '2018-04-08 17:45:41', '2018-04-08 17:45:41'),
(3, 'narys', 'narys@narys.lt', '$2y$10$cioDEE7ZHJOELytQgSXXfejdjnytO0Y1lN5A6gz1yq/mjJJLcg5ta', 'bFpIyi3Ba3wOClHHnlsHTKka1AT7d2rRxUDu5pCr2ti5DrZJwh7XbBRCDfZl', '2018-04-09 17:48:20', '2018-04-09 17:48:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tasks_email_unique` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
